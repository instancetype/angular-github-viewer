/**
 * Created by instancetype on 11/29/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
;(function STRICT(angular) {
  'use strict';

  var app = angular.module('app')

  var RepoController = function RepoController($scope, github, $routeParams) {
    var onDetailSuccess
      , onContributorSuccess
      , onDetailsFailure
      , onContribFailure
      , username
      , reponame


    onDetailSuccess = function onDetailSuccess(data) {
      $scope.detailsFailure = ''
      $scope.details = data
    }

    onContributorSuccess = function onContributorSuccess(data) {
      $scope.contribFailure = ''
      $scope.contributors = data
    }

    onDetailsFailure = function onDetailsFailure() {
      $scope.detailsFailure = 'Could not retrieve data'
    }

    onContribFailure = function onContribFailure() {
      $scope.contribFailure = 'Could not retrieve data'
    }

    username = $scope.username = $routeParams.username
    reponame = $routeParams.reponame

    github.getRepoDetails(username, reponame).then(onDetailSuccess, onDetailsFailure)
    github.getRepoContributors(username, reponame).then(onContributorSuccess, onContribFailure)
  }

  app.controller('RepoController', RepoController)
}(angular))