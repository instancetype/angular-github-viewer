/**
 * Created by instancetype on 11/29/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
;(function STRICT(angular) {
  'use strict';

  var app = angular.module('app', ['ngRoute'])

  app.config(function appConfig($routeProvider) {

    $routeProvider
      .when('/main', {
        templateUrl : 'main.html'
      , controller  : 'MainController'
      })
      .when('/user/:username', {
        templateUrl : 'user.html'
      , controller  : 'UserController'
      })
      .when('/user/:username/:reponame', {
        templateUrl : 'repo.html'
      , controller  : 'RepoController'
      })
      .otherwise({ redirectTo : '/main' })
  })

}(angular))