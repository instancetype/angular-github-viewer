/**
 * Created by instancetype on 11/28/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
;(function STRICT(angular) {
  'use strict';

  var module = angular.module('app')

  var github = function github($http) {
    var getUser
      , getRepos
      , getRepoDetails
      , getRepoContributors

    getUser = function getUser(username) {
      var url = 'https://api.github.com/users/' + username
      return $http.get(url)
        .then(function(response) {
          return response.data
        })
    }

    getRepos = function getRepos(user) {
      return $http.get(user.repos_url)
        .then(function(response) {
          return response.data
        })
    }

    getRepoDetails = function getRepoDetails(username, reponame) {
      var url = 'https://api.github.com/repos/' + username + '/' + reponame
      return $http.get(url).then(function(response) {
        return response.data
      })
    }

    getRepoContributors = function getRepoContributors(username, reponame) {
      var url = 'https://api.github.com/repos/' + username + '/' + reponame + '/contributors'
      return $http.get(url).then(function(response) {
        return response.data
      })
    }

    return {
      getUser  : getUser
    , getRepos : getRepos
    , getRepoDetails : getRepoDetails
    , getRepoContributors : getRepoContributors
    }
  }

  module.factory('github', github)
}(angular))