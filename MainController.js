/**
 * Created by instancetype on 11/28/14.
 */
/* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
 asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
;(function iffy(angular) {
  'use strict';

  var app = angular.module('app')

  var MainController = function MainController($scope, $interval, $location) {
    var decrementCountdown
      , startCountdown
      , interval

    decrementCountdown = function decrementCountdown() {
      if (--$scope.countdown === 0) {
        $scope.getUserData($scope.username)
      }
    }

    startCountdown = function startCountdown() {
      interval = $interval(
        decrementCountdown
      , 1000
      , $scope.countdown // number of intervals to execute
      )
    }

    $scope.getUserData = function getUserData(username) {
      if (interval) {
        $interval.cancel(interval)
        $scope.countdown = 0
      }
      $location.path('/user/' + username)
    }

    $scope.username = 'angular'
    $scope.countdown = 10

    startCountdown()
  }

  app.controller('MainController', MainController)
}(angular))