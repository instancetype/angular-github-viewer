/**
 * Created by instancetype on 11/28/14.
 */
/* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
 asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
;(function iffy(angular) {
  'use strict';

  var app = angular.module('app')

  var UserController = function MainController($scope, github, $routeParams) {
    var onUserSuccess
      , onRepoSuccess
      , onFailure

    onRepoSuccess = function(data) {
      $scope.repos = data
    }

    onUserSuccess = function onUserSuccess(data) {
      $scope.failure = ''
      $scope.user = data

      github.getRepos($scope.user).then(onRepoSuccess, onFailure)
    }

    onFailure = function onFailure() {
      $scope.user = ''
      $scope.failure = 'Could not retrieve data'
    }

    $scope.username = $routeParams.username
    $scope.repoSortOrder = '-stargazers_count'

    github.getUser($scope.username).then(onUserSuccess, onFailure)
  }

  app.controller('UserController', UserController)
}(angular))